import { Component, OnInit } from '@angular/core';
import { PusherService } from '../pusher-services/pusher.service';


@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})

export class MessagesComponent implements OnInit {
  userName;
  messageText;
  messages: Array<Message>;
 
  constructor(private pusherService: PusherService) {

    this.messages=[]
   }
   
   sendMessage(user: string, text: string) {
    const message: Message = {

       user: user,
       text: text,

      
    }
    this.pusherService.messagesChannel.trigger('client-new-message', message);
    this.messages.push(message);
  }
  ngOnInit(): void{

   
    //listening the new messeges
    this.pusherService.messagesChannel.bind('client-new-message', (message) => {
      this.messages.push(message);
  });

}

}

interface Message {
  text: string;
  user: string;
}
